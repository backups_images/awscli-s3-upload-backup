# Backup example:

docker run --rm -i \
  --name backup_export_container \
  --net database_network \
  -v $(pwd)/tmp/:/tmp/:rw \
  mariadb:latest \
  bash -c "
  if [ ! -d /tmp/backup/ ]; then
    mkdir /tmp/backup/
  fi

  if [ -f /tmp/backup/*.sql ] || [ -f /tmp/backup/*.gz ] || [ -f /tmp/backup/*.tar.gz ];
  then
    rm -R /tmp/backup/*
    echo '/tmp/backup/ folder clean (done)'
  fi;

  if ! mysqldump --single-transaction --quick --user=homestead --password=secret -h mariadb_container homestead > /tmp/backup/database.sql 2>database.err
  then
    echo 'mysqldusmp fail'
    echo '---------------'
    cat database.err
    exit 1;
  fi
  " && \
\
docker run --rm -i \
  --name awscli_s3_upload \
  --net database_network \
  -v $(pwd)/tmp/:/tmp/:rw \
  -e DOMAIN=alita.ee \
  -e AWS_ACCESS_KEY_ID=???? \
  -e AWS_SECRET_ACCESS_KEY=???? \
  -e AWS_BUCKET=alita.ee-database \
  -e AWS_REGION=eu-west-2 \
  registry.gitlab.com/backups_images/awscli-s3-upload
