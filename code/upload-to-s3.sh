#!/bin/sh

# Configuration
DOMAIN="${DOMAIN}_"
CURRENT_DATE=$(date +'%Y.%m.%d-%H.%M.%S')
ARCHIVE_NAME="$DOMAIN$CURRENT_DATE"

# Archive
tar czf /tmp/$ARCHIVE_NAME.tar.gz -C /tmp/backup/ .

# Get filesize
FILE_SIZE=$(wc -c < /tmp/$ARCHIVE_NAME.tar.gz)

# Check archive size, if archive size is more than 2kb, it will upload to aws s3.
if [ $FILE_SIZE -ge 2000 ]; then
	aws s3 cp /tmp/$ARCHIVE_NAME.tar.gz s3://$AWS_BUCKET/$ARCHIVE_NAME.tar.gz > output
	if [ $? -eq 0 ]; then
		rm -R /tmp/*;
	else
		rm -R /tmp/*;
	    echo 'awscli s3 - upload fail'
	    echo '---------------'
	    exit 1;
	fi
else
	rm -R /tmp/*;
    echo 'Strange file size'
    echo '---------------'
    exit 1;
fi
